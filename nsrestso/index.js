const config =require('./config.json');
const NetSuiteOauth = require('netsuite-tba-oauth');
const url =require ('url');
const path= '/services/rest/query/v1/suiteql?' //limit=1&offset=8'
const {DefaultAzureCredential, ManagedIdentityCredential} = require('@azure/identity');
const {SecretClient} = require('@azure/keyvault-secrets');
// // DefaultAzureCredential expects the following three environment variables:
// // - AZURE_TENANT_ID: The tenant ID in Azure Active Directory
// // - AZURE_CLIENT_ID: The application (client) ID registered in the AAD tenant
// // - AZURE_CLIENT_SECRET: The client secret for the registered application
 const credential = new DefaultAzureCredential();
var http = require('http');
// ManagedIdentityCredential created by "identity assign" command
//const credential = new ManagedIdentityCredential();
module.exports = async function (context, req) {
    
    const vaultName = "EuropeClientServicesKeys";
    const vaulturl = 'https://'+vaultName+'.vault.azure.net';
    var query ="SELECT to_char(SYSDATE,'yyyymmddhh24miss') as exported_date,customer.entityid AS CustomerId, customer.altname AS CustomerName, transaction.tranid AS SalesOrder, transaction.entity AS CustomerInternalId, transaction.shippingaddress AS ShippingAddressInternalId, transaction.shipdate AS SalesOrderDate, transaction.lastmodifieddate, transaction.otherrefnum, subsidiary.name AS subsidiary,transactionline.item AS ItemInternalId, item.itemid AS Item, transactionline.quantity, transactionline.custcol_sf_billing_schedule AS BillingSchedule, transactionline.memo, transactionline.linelastmodifieddate, transaction.tranid || '-' || transactionline.linesequencenumber AS SalesOrderLineId, salesordershippingaddress.country AS CustomerLocation FROM transaction inner join transactionline on transactionline.transaction = transaction.id inner join item on item.id = transactionline.item inner join customer on customer.id = transaction.entity inner join salesordershippingaddress on salesordershippingaddress.nkey = transaction.shippingaddress inner join subsidiary on subsidiary.id = transactionline.subsidiary WHERE transaction.type = 'SalesOrd' AND item.incomeaccount IN (685, 678) AND transactionline.rate > 0  AND item.incomeaccount IS NOT NULL AND transactionline.memo NOT LIKE '%Managed%'"
 
    const client = new SecretClient(vaulturl, credential);

    //const name = (req.query.name || (req.body && req.body.name));
    // retrieve Parameters in the body 
    const modifieddate=(req.query.modifieddate || (req.body && req.body.modifieddate));
    const limit = (req.query.limit || (req.body && req.body.limit));
    const offset = (req.query.offset || (req.body && req.body.offset));
    const nsquery = (req.query.nsquery || (req.body && req.body.nsquery));

    context.log('limit:'+limit);
    context.log('offset:'+offset);
    // if query is in the body replace the default one
    if (nsquery)
        query =nsquery;
    var vaultprefix = (req.query.vaultprefix || (req.body && req.body.vaultprefix));
    if (!vaultprefix)
        vaultprefix='ns-staging-';
    context.log ("Vaultprefix:"+vaultprefix);

    const NSCredentials={ 
        "account":{
            "host": (await client.getSecret(vaultprefix+'account-host')).value,
            "id" :(await client.getSecret(vaultprefix+'account-id')).value,
        },
        "consumer":{
            "key":(await client.getSecret(vaultprefix+'consumer-key')).value,
            "secret":(await client.getSecret(vaultprefix+'consumer-secret')).value,
        },
        "token":{
            "key":(await client.getSecret(vaultprefix+'token-key')).value,
            "secret":(await client.getSecret(vaultprefix+'token-secret')).value,
        }

    }    
    
    //context.log(NSCredentials)

    var apiurl ='https://'+NSCredentials.account.host;
    let nsresponse ={};
    var paging ={};
    // const returnitems=[];      
    // const timestamp = DateTime.utc();
    // console.log(context.req.originalUrl)
    const originalurl = url.parse(context.req.originalUrl);
    // if modified date modify query 
      if (modifieddate){
    query+=" AND to_char(transaction.lastmodifieddate, 'yyyymmddhh24miss') > '"+modifieddate+"'";
    context.log('modifieddate '+modifieddate)
    }
     // if parameter limit and offset else 1 record
    if (limit && offset) {
        apiurl+=path+'limit='+limit +'&'+'offset='+offset
    } else {
        apiurl+=path+'limit=100&offset=0'
    }
    //context.log (query);
    // Use Config file
    //const oauth = new NetSuiteOauth(apiurl, 'POST', config.consumer.key, config.consumer.secret, config.token.key, config.token.secret, config.account.id);
    // Use KeyVault
    const oauth = new NetSuiteOauth(apiurl, 'POST', NSCredentials.consumer.key, NSCredentials.consumer.secret, NSCredentials.token.key, NSCredentials.token.secret, NSCredentials.account.id);

    // context.log(oauth)
    oauth.headers['Prefer']='transient';
    await oauth.post({ q: query}
    ).then(response => {
        //nsresponse=response;
       // context.log(response);
       // Change the url of all the link 
       response.links.forEach(element => {
        element.href =element.href.replace(NSCredentials.account.host+path,originalurl.host+originalurl.pathname+'?');
        //console.log(element.rel);
        paging[element.rel]=element.href;

        });
        // Add exported date to the json with the NSUITE Timezone 
        // Commented as the exported date is retrieved by the query
        /*response.items.forEach(element => {
            let nsdate =element['timezone'];
            let offzone=nsdate.substr(15,3)
            element['exported_date']=timestamp.setZone('UTC'+offzone).toFormat('yyyymmddHHmmss'); 
            returnitems.push(element)
        })*/
        // Loop on all value of the json
        //const parsedJSON = JSON.parse(response);
        Object.keys(response).forEach(element =>  {
            if (!Array.isArray(response[element])) {
                nsresponse[element]=response[element];
            }
        })
        nsresponse['links']=response.links;
        
        nsresponse['items']=response.items;
        nsresponse['paging'] =paging;
        //context.log(timestamp)   

        
            context.res = {
                // status: 200, /* Defaults to 200 */
                body: nsresponse
            }
          ; 
       /* for  (const element of nsresponse.items) {
            context.log(element.customerid);
                    element['exported_date']=timestamp;        
            };   */       
            context.done(null, nsresponse);

    }
    )
}