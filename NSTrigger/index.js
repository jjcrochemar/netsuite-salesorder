const config =require('./config.json');
const NetSuiteOauth = require('netsuite-tba-oauth');
const url =require ('url');
const path= '/services/rest/query/v1/suiteql?' //limit=1&offset=8'
module.exports = async function (context, req) {
    
    //context.log(config.account);
    //context.log('JavaScript HTTP trigger function processed a request.');

    const name = (req.query.name || (req.body && req.body.name));
    const limit = (req.query.limit || (req.body && req.body.limit));
    const offset = (req.query.offset || (req.body && req.body.offset));
    var apiurl ='https://'+config.account.host;
    let nsresponse ='';
    var paging ={};
    // console.log(context.req.originalUrl)
    const originalurl = url.parse(context.req.originalUrl);
    // if parameter limit and offset else 1 record
    if (limit && offset) {
        apiurl+=path+'limit='+limit +'&'+'offset='+offset
    } else {
        apiurl+=path+'limit=1&offset=0'
    }
    const oauth = new NetSuiteOauth(apiurl, 'POST', config.consumer.key, config.consumer.secret, config.token.key, config.token.secret, config.account.id);
    // context.log(oauth)
    oauth.headers['Prefer']='transient';
    await oauth.post({ q:"SELECT customer.entityid AS CustomerId, customer.altname AS CustomerName, transaction.tranid AS SalesOrder, transaction.entity AS CustomerInternalId, transaction.shippingaddress AS ShippingAddressInternalId, transaction.shipdate AS SalesOrderDate, transaction.lastmodifieddate, transaction.otherrefnum, subsidiary.name AS subsidiary,transactionline.item AS ItemInternalId, item.itemid AS Item, transactionline.quantity, transactionline.custcol_sf_billing_schedule AS BillingSchedule, transactionline.memo, transactionline.linelastmodifieddate, transaction.tranid || '-' || transactionline.linesequencenumber AS SalesOrderLineId, salesordershippingaddress.country AS CustomerLocation FROM transaction inner join transactionline on transactionline.transaction = transaction.id inner join item on item.id = transactionline.item inner join customer on customer.id = transaction.entity inner join salesordershippingaddress on salesordershippingaddress.nkey = transaction.shippingaddress inner join subsidiary on subsidiary.id = transactionline.subsidiary WHERE transaction.type = 'SalesOrd' AND item.incomeaccount IN (685, 678) AND transactionline.rate > 0  AND item.incomeaccount IS NOT NULL AND transactionline.memo NOT LIKE '%Managed%'"}
    ).then(response => {nsresponse=response;
       // context.log(response);
        nsresponse.links.forEach(element => {
        element.href =element.href.replace(config.account.host+path,originalurl.host+originalurl.pathname+'?');
        //console.log(element.rel);
        paging[element.rel]=element.href;

        });
        nsresponse['paging'] =paging
    context.res = {
        // status: 200, /* Defaults to 200 */
        body: nsresponse
    }
    context.done(null, nsresponse);

    }
    )
}